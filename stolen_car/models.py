from django.db import models


# Create your models here.

class CarBrand(models.Model):
    name = models.CharField(max_length=128, primary_key=True, unique=True)


class CarModel(models.Model):
    name = models.CharField(max_length=128)
    brand = models.ForeignKey(CarBrand, related_name='car_models', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('name', 'brand',)


class StolenCar(models.Model):
    owner = models.CharField(max_length=128)
    licence_plate_number = models.CharField(max_length=32)
    vin_code = models.CharField(max_length=32)
    color = models.CharField(max_length=32)
    model = models.ForeignKey(CarModel, related_name='stolen_cars', on_delete=models.CASCADE)
    manufactured_at = models.PositiveIntegerField()
    stolen_at = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
