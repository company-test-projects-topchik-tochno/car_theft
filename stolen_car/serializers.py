from django.utils import timezone
from requests import HTTPError
from urllib3 import HTTPSConnectionPool

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from stolen_car.models import StolenCar, CarBrand, CarModel
from stolen_car.services.car_catolog_service import VehicleAPIService


class StolenCarGetSerializer(serializers.ModelSerializer):
    model = serializers.CharField(source='model.name')
    brand = serializers.CharField(source='model.brand.name')

    class Meta:
        model = StolenCar
        fields = (
            'id',
            'owner',
            'color',
            'brand',
            'model',
            'vin_code',
            'stolen_at',
            'created_at',
            'updated_at',
            'manufactured_at',
            'licence_plate_number',

        )


class StolenCarCreateUpdateSerializer(serializers.ModelSerializer):
    # needed to map received fields to model fields
    car_info_field_to_car_model_field_map = {
        'Make': 'brand',
        'Model': 'name',
        'Model Year': 'manufactured_at'
    }

    class Meta:
        model = StolenCar
        fields = (
            'id',
            'owner',
            'color',
            'vin_code',
            'stolen_at',
            'licence_plate_number',
        )

    def validate_brand(self, brand):
        if not CarBrand.objects.filter(name=brand).exists():
            raise ValidationError("Brand doesn't exist")
        return brand

    def validate_model(self, model):
        if not CarModel.objects.filter(name=model).exists():
            raise ValidationError("Model doesn't exist")
        return model

    def validate_stolen_at(self, stolen_at):
        if stolen_at > timezone.now().date():
            raise ValidationError('Car can"t be stolen in future')
        return stolen_at

    def validate(self, attrs):
        try:
            car_info = VehicleAPIService.get_car_info_by_vin_code(attrs['vin_code'])
        except HTTPSConnectionPool:
            raise ValidationError(code=503, detail='service temporarily unavailable')
        except HTTPError as e:
            raise ValidationError(e)

        # remain only info that we need
        car_info = filter(lambda info: info['Variable'] in self.car_info_field_to_car_model_field_map.keys(), car_info)
        model_fields_dict = {}

        for info in car_info:
            # validate car data and prepare dict with data received by vin code
            # for updating or creating instance after validation
            field = self.car_info_field_to_car_model_field_map[info['Variable']]
            validator = getattr(self, f"validate_{field}", None)
            value = validator(info['Value']) if validator else info['Value']
            model_fields_dict[field] = value

        # check if model present in our system
        car_model = CarModel.objects.filter(name=model_fields_dict['name'], brand_id=model_fields_dict['brand'])
        if not car_model.exists():
            raise ValidationError('such car model doesn"t exist')
        print(model_fields_dict)
        # update attrs with missing data so the model can be created or updated
        attrs.update({
            'model': car_model.get(),
            'manufactured_at': model_fields_dict['manufactured_at']
        })
        return attrs


class CarModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarModel
        fields = ('name', 'id')


class CarBrandsWithModelsListSerializer(serializers.ModelSerializer):
    car_models = CarModelSerializer(many=True)

    class Meta:
        model = CarBrand
        fields = ('name', 'car_models',)
