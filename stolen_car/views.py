import django_filters
from django.http import HttpResponse

from rest_framework import filters
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView

from stolen_car.models import StolenCar, CarBrand
from stolen_car.services.car_catolog_service import StolenCarExportService
from stolen_car.serializers import StolenCarCreateUpdateSerializer
from stolen_car.serializers import StolenCarGetSerializer
from stolen_car.serializers import CarBrandsWithModelsListSerializer
from stolen_car.tasks import update_car_catalog


class StolenCarViewSet(ModelViewSet):
    queryset = StolenCar.objects.all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['vin_code', 'owner', 'licence_plate_number']
    filter_fields = ['model__name', 'model__brand__name', 'manufactured_at']
    http_method_names = ['get', 'post', 'put', 'options', 'delete']
    ordering_fields = [
        'owner', 'model__name', 'model__brand__name',
        'color', 'manufactured_at', 'vin_code',
        'stolen_at', 'licence_plate_number'
    ]

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return StolenCarGetSerializer
        return StolenCarCreateUpdateSerializer

    @action(detail=False, methods=['get'])
    def export(self, request):
        stolen_cars = self.filter_queryset(self.get_queryset())
        file = StolenCarExportService.export_stolen_cars(stolen_cars)

        with open(file.filename, 'rb') as xls_file:
            data = xls_file.read()

        response = HttpResponse(data, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = f'attachment; filename={file.filename}'
        return response


class CarListView(ListAPIView):
    serializer_class = CarBrandsWithModelsListSerializer

    def get_queryset(self):
        # for all brands and models at once
        queryset = CarBrand.objects.all()
        brand = self.request.query_params.get('brand')

        # for dynamic brand getting (for autocomplete)
        if brand:
            queryset = queryset.filter(name__istartswith=brand)

        return queryset
