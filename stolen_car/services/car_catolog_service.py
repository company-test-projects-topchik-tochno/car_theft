import base64

from django.db import transaction
from requests import request
from xlsxwriter.workbook import Workbook

from stolen_car.models import CarBrand, CarModel, StolenCar


class VehicleAPIService:
    BASE_URL = 'https://vpic.nhtsa.dot.gov/api'

    @classmethod
    def _make_request(cls, method, url, data=None):
        response = request(method=method, url=url, data=data)
        response.raise_for_status()
        return response.json()

    @classmethod
    def get_car_brands(cls):
        url = f"{cls.BASE_URL}/vehicles/getMakesForVehicleType/car?format=json"
        return cls._make_request(url=url, method='GET')['Results']

    @classmethod
    def get_car_models_by_brand(cls, brand_id):
        url = f"{cls.BASE_URL}/vehicles/getmodelsformakeid/{brand_id}?format=json"
        return cls._make_request(url=url, method='GET')['Results']

    @classmethod
    def get_car_info_by_vin_code(cls, vin_code):
        url = f"{cls.BASE_URL}/vehicles/decodevin/{vin_code}?format=json"
        return cls._make_request(url=url, method='GET')['Results']


class CarCatalogService:

    @classmethod
    def update_car_catalog(cls) -> None:
        car_brands = VehicleAPIService.get_car_brands()
        cls.update_car_brands(
            [brand['MakeName'] for brand in car_brands]
        )
        for brand in car_brands:
            car_models = VehicleAPIService.get_car_models_by_brand(brand['MakeId'])
            car_models = [model['Model_Name'] for model in car_models]
            cls.update_car_models_by_brand(brand['MakeName'], car_models)

    @classmethod
    def update_car_brands(cls, car_brands: [str]) -> None:
        existing_brands = set(CarBrand.objects.all().values_list('name', flat=True))
        car_brands = set(car_brands)
        car_brands_to_add = car_brands - existing_brands
        if car_brands_to_add:
            with transaction.atomic():
                CarBrand.objects.bulk_create(
                    [CarBrand(name=brand) for brand in car_brands_to_add]
                )

    @staticmethod
    def update_car_models_by_brand(car_brand: str, car_models: [str]) -> None:
        existing_models = set(CarModel.objects.filter(brand=car_brand).values_list('name', flat=True))
        car_models = set(car_models)
        car_models_to_add = car_models - existing_models

        if car_models_to_add:
            with transaction.atomic():
                CarModel.objects.bulk_create(
                    [CarModel(name=model, brand_id=car_brand) for model in car_models_to_add]
                )


class StolenCarExportService:
    @classmethod
    def export_stolen_cars(cls, stolen_car_qs: [StolenCar]):
        data = cls._get_data_for_export(stolen_car_qs)
        return cls._perform_file_creation(data)

    @classmethod
    def _get_data_for_export(cls, stolen_car_qs):
        data = {0: [
            'owner',
            'color',
            'model',
            'brand',
            'vin_code',
            'stolen_at',
            'manufactured_at',
            'licence_plate_number',
        ]}

        for number, stolen_car in enumerate(stolen_car_qs, 1):
            data[number] = [
                stolen_car.owner,
                stolen_car.color,
                stolen_car.model.name,
                stolen_car.model.brand.name,
                stolen_car.vin_code,
                str(stolen_car.stolen_at),
                stolen_car.manufactured_at,
                stolen_car.licence_plate_number,
            ]

        return data

    @classmethod
    def _perform_file_creation(cls, data):
        res_file = Workbook('stolen_cars.xls')

        row = 0
        sheet = res_file.add_worksheet()
        for record_number in data:
            for col_number in range(len(data[record_number])):
                sheet.write(row, col_number, data[record_number][col_number])
            row += 1

        res_file.close()

        return res_file
