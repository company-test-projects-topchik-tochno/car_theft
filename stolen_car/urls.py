from stolen_car.views import StolenCarViewSet, CarListView
from rest_framework.routers import SimpleRouter
from django.urls import path

app_name = 'stolen-cars'

router = SimpleRouter()
router.register('stolen-cars', StolenCarViewSet)

urlpatterns = [
    path('brands/', CarListView.as_view()),
]

urlpatterns += router.urls
