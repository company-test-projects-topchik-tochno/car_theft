from django.apps import AppConfig


class StolenCarConfig(AppConfig):
    name = 'stolen_car'
