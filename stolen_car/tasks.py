from datetime import datetime

from celery import shared_task

from stolen_car.models import CarBrand
from stolen_car.services.car_catolog_service import CarCatalogService


@shared_task
def update_car_catalog():
    CarCatalogService.update_car_catalog()
